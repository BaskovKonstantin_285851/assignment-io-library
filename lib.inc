%define EX 60
%define TAB 0x21
%define ENDL 0xca
%define ONE 1
%define STDOUT 1
%define SYS_WRITE 1
%define ZERO 0

exit: 
    mov rax, EX
    syscall     

string_length:
    xor rax, rax
	.loop:
		cmp byte [rdi+rax], ZERO
		je .end
		inc rax
		jmp .loop
	.end:
		ret

print_string:
    push rdi
	call string_length
	pop rsi
	mov rdx,rax
	mov rax, STDOUT
	mov rdi, SYS_WRITE
	syscall
	xor rax,rax 
    ret

print_newline:
	mov rsp, 10

print_char:
	push rdi
	mov rsi,rsp 
	mov rdi, ONE
	mov rdx, ONE
	mov rax, ONE
	syscall
    pop rdi
	ret



print_int:
	mov rax, rdi
	test rax,rax
	jns print_uint
	mov rdi, '-'
	push rax
	call print_char
	pop rax
	neg rax
	mov rdi, rax


print_uint:
	push r12
    push r14
    mov r14,10
	mov r12, rsp
	mov rax, rdi
	dec rsp
	mov byte[rsp], ZERO
	.loop:
		dec rsp
		xor rdx,rdx
		div r14
		add rdx,0x30
		mov byte[rsp], dl
		test rax, rax
		jz .print
		jmp .loop
	.print:
		mov rdi, rsp
		call print_string
		mov rsp, r12
	pop r12
    pop r14
    ret





string_equals:
	call string_length
	mov rcx, rax
	xchg rdi, rsi
	call string_length
	cmp rax, rcx 
	jne .not_equals
	repe cmpsb
	jne .not_equals
	mov rax, ONE
    ret
	.not_equals:
		xor rax, rax
		ret

read_char:
    dec rsp 
	mov rax,ZERO 
	mov rdi,ZERO 
	mov rsi,rsp 
	mov rdx,ONE 
	syscall 
	mov rax, [rsp] 
	inc rsp 
	ret 



read_word:
    push rbx 
    	mov r8, rsi 
    	mov r9, rdi 
    	xor r10, r10 
    	push rdi 
    	xor rdi, rdi 
    	mov rdx, ONE
    	.next:
    		inc r10 
        	xor rax, rax 
        	mov rsi, r9 
        	syscall
    		cmp r10, r8 
    		je .end ;
        	cmp byte [r9], ENDL 
        	je .end ;
        	cmp byte [r9], TAB 
        	jb .next 
    	.again:
        	dec r8
        	cmp r8, ONE
        	je .overflow
        	inc r9
        	xor rax, rax
        	mov rsi, r9
        	syscall
        	cmp byte [r9], TAB
        	jb .end
        	cmp byte [r9], ENDL
        	je .end
        	jmp .again
    	.end:
        	mov byte [r9], ZERO
        	pop rax
       		mov rdx, r9
        	sub rdx, rax
        	pop rbx
        	ret
    	.overflow:
        	pop rax
        	mov rax, ZERO
        	mov rdx, ZERO
        	pop rbx
        	ret
 


parse_uint:
    call string_length 
	mov rcx,rax   
	mov rsi,rdi 
	xor rdx,rdx 
	xor rax,rax 
	.parse: 
		xor rdi,rdi 
		mov dil,byte[rsi+rdx] 
		cmp dil,'0' 
		jb .end 
		cmp dil,'9' 
		ja .end 
		sub dil,'0' 
		imul rax,10 
		add rax,rdi 
		inc rdx 
		dec rcx 
		jnz .parse
	.end:
		ret


parse_int:
    cmp byte[rdi],'-' 
	je .minus 
	jne parse_uint 
	.minus:
		inc rdi 
		call parse_uint 
		test rdx,rdx 
		jz .null 
		neg rax 
		inc rdx 
		ret
	.null:
		xor rax,rax 
		ret


string_copy:
    xor rax, rax 
	xor r9, r9 
	xor rcx, rcx 
    	call string_length 
    	push rax 
    	push rsi 
	.loop:
    		cmp rcx, rdx 
		je .error
    		mov r8b, [rdi+rcx] 
    		mov [rsi+rcx], r8b 
    		test rax, 0 
    		cmp rax, 0 
    		je .end 
    		dec rax 
    		inc rcx 
    		jmp .loop 
	.end:
		pop rsi 
		pop rax 
    	mov byte [rsi+rax], 0 
    	jmp .exit	
	.error:
    		pop rsi 
		pop rax 
		mov rax, 0 
	.exit
		ret
